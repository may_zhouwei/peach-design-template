
<div align="center">
<br/>

  <h1 align="center">
     :peach: :peach: :peach:  Peach-Design-Template
  </h1>
  <h4 align="center">
    最 佳 实 践 的  Vue3  前 端  框 架
  </h4> 

</div>


#### 项目介绍   

 Vue3 + Ant Design Vue 2 + Axios 前端模板，提供快速开发

 项目不定时更新，建议 Star watch 一份



#### 快速上手

```

// 依赖安装

npm i


// 运行项目

npm run serve


// 构建项目
npm run build

```


## Pro演示地址

- [⚡️ vue3.x + Antd（支持 PC、平板、手机）](http://may_zhouwei.gitee.io/peach-design-pro)

## Template演示地址

- [⚡️ vue3.x +Antd（免费商用，支持 PC、平板、手机）](http://may_zhouwei.gitee.io/peach-design-template)



## Peach-Design QQ 群

- 遇到问题请加qq群 1046639311
<br />

<table>
<tr>
<td>
<img src='https://images.gitee.com/uploads/images/2021/0531/151904_d92601f0_7443633.jpeg' />
</td>
<td>
<img src='https://images.gitee.com/uploads/images/2021/0531/151904_d92601f0_7443633.jpeg' />
</td>
</tr>
</table>




```bash
# 克隆项目
git clone -b master https://gitee.com/may_zhouwei/peach-design-template.git 
```


## 鸣谢

| 特别鸣谢|
| ---------------------------------------------------------------- |
| [vue](https://github.com/vuejs/vue)                              |  |
| [ant-design-vue](https://github.com/vueComponent/ant-design-vue) |
| [mock](https://github.com/nuysoft/Mock)                          |
| [axios](https://github.com/axios/axios)                          |



## 适合人群

- 正在以及想使用 Antd 开发，学习VUE3的朋友。
- 熟悉 Vue.js 技术栈，使用它开发过几个实际项目。
- 对原理技术感兴趣，想进阶和提升的同学。



## 效果图

以下是截取的是 pro 版的效果图展示：

<table>
<tr>
<td>
<img src='https://images.gitee.com/uploads/images/2021/0510/170306_e766c33c_7443633.jpeg' />
</td>
<td>
<img src="https://images.gitee.com/uploads/images/2021/0510/170438_15511f92_7443633.png">
</td>
</tr>
<tr>
<td>
<img src="https://images.gitee.com/uploads/images/2021/0510/170502_61b15af1_7443633.jpeg">
</td>
<td>
<img src="https://images.gitee.com/uploads/images/2021/0510/170521_bea9f3b5_7443633.png">
</td>
</tr>
<tr>
<td>
<img src="https://images.gitee.com/uploads/images/2021/0510/170541_5a497953_7443633.jpeg">
</td>
<td>
<img src="https://images.gitee.com/uploads/images/2021/0510/170609_8380f0ac_7443633.png">
</td>
</tr>

<tr>
<td>
<img src="https://images.gitee.com/uploads/images/2021/0510/170637_21c70101_7443633.jpeg">
</td>
<td>
<img src="https://images.gitee.com/uploads/images/2021/0510/170655_a5ee91e8_7443633.png">
</td>
</tr>
</table>

## 商用注意事项

此项目可免费用于商业用途，无任何限制！

