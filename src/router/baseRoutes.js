import Layout from "@/layouts/index.vue";
import EmptyLayout from "@/layouts/EmptyLayout";
export default [
  {
    path: "/",
    redirect: "/home/index",
    hidden: true
  },
  {
    path: "/login",
    component: () => import("@/view/user/login.vue"),
    hidden: true
  },
  {
    path: "/user",
    name: "userCenter",
    component: Layout,
    meta: {
      title: "个人中心",
      icon: "HomeOutlined",
    },
    hidden: true,
    children: [
      {
        path: "center",
        name: "user-center",
        meta: {
          title: "个人中心",
          icon: "DashboardOutlined",
        },
        component: () => import("@/view/user/center.vue")
      }
    ]
  },
  //首页
  {
    path: "/home",
    name: "A001",
    meta: {
      title: "工作空间",
      icon: "HomeOutlined",
      alwaysShow: false,
    },
    component: Layout,
    alwaysShow: false,
    children: [
      {
        path: "index",
        name: "A002",
        meta: {
          title: "分析页",
          icon: "DashboardOutlined",
          fixed: true
        },
        component: () => import("@/view/home/index.vue")
      }
    ]
  },
  {
    hidden: false,
    path: "/users",
    name: "users",
    meta: {
      title: "用户管理",
      icon: "UserOutlined",
    },
    component: Layout,
    children: [
      {
        path: "role",
        name: "role",
        meta: {
          title: "角色管理",
          icon: "DashboardOutlined",
        },
        component: () => import("@/view/users/role/index.vue")
      },
      {
        path: "user",
        name: "user",
        meta: {
          title: "用户管理",
          icon: "DashboardOutlined",
        },
        component: () => import("@/view/users/user/index.vue")
      }
    ]
  },
  {
    hidden: true,
    path: "/error",
    name: "error",
    meta: {
      title: "错误页面",
      icon: "StopOutlined",
    },
    component: Layout,
    redirect: "/error/404",
    children: [
      {
        path: "500",
        name: "error-500",
        meta: {
          title: "500",
          icon: "DatabaseOutlined",
        },
        component: () => import("@/view/error/500.vue")
      },
      {
        path: "403",
        name: "error-403",
        meta: {
          title: "403",
          icon: "DatabaseOutlined",
        },
        component: () => import("@/view/error/403.vue")
      },
      {
        path: "404",
        name: "error-404",
        meta: {
          title: "404",
          icon: "DatabaseOutlined",
        },
        component: () => import("@/view/error/404.vue")
      }
    ]
  }
];
